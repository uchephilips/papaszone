<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AppUsers */
/* @var $businessProfile app\models\AppUserBusinessProfile */

$this->title = "User Profile - " . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'App Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-xl-8 order-xl-1">
        <div class="card bg-secondary shadow">
            <div class="card-header bg-white border-0">
                <div class="row align-items-center">
                    <div class="col-8">
                        <h6 class="text-uppercase text-muted ls-1 mb-1">Bold Transaction</h6>
                        <h2 class="mb-0"><?= $this->title ?></h2>
                        <span class="badge badge-dot mr-4">
                            <?php if ($model->is_active) { ?>
                                <i class="bg-success"></i>
                            <?php } else { ?>
                                <i class="bg-danger"></i>
                            <?php } ?>
                            <?= $model->is_active ? "Active" : "Suspended" ?>
                        </span>
                    </div>
                    <div class="col-md-4 text-right">
                        <a href="<?= Yii::getAlias('@web'); ?>/app-users" class="btn btn-lg btn-default"><i class="fa fa-arrow-circle-left"></i> Back</a>
                        <div class="dropdown">
                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                <a class="dropdown-item" href="<?= Yii::getAlias('@web'); ?>/app-users/update?id=<?= $model->id ?>">Update</a>
                                <a class="dropdown-item" href="<?= Yii::getAlias('@web'); ?>/app-users/suspend?id=<?= $model->id ?>"
                                   onclick="return confirm('Are you sure you want to make this action ?')"><?= $model->is_active == 1 ? 'Suspend' : "Unsuspend" ?></a>
                                <a class="dropdown-item" href="<?= Yii::getAlias('@web'); ?>/app-users/reset-password?id=<?= $model->id ?>">Reset password</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <h6 class="heading-small text-muted mb-4">Transaction information</h6>
                <div class="pl-lg-4">
                    <div class="portlet-body">
                        <div class="row mb-2" >
                            <div class="col-md-5 name">
                                <strong>User ID:</strong>
                            </div>
                            <div class="col-md-7 value">
                                <?= $model->id ?>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-5 name">
                                <strong>Name:</strong>
                            </div>
                            <div class="col-md-7 value">
                                <?= $model->first_name . '  ' . $model->middle_name . ' ' . $model->last_name ?>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-5 name">
                                <strong>Phone:</strong>
                            </div>
                            <div class="col-md-7 value">
                                <?= $model->phone_number ?>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-5 name">
                                <strong>Email:</strong>
                            </div>
                            <div class="col-md-7 value">
                                <?= $model->email ?>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-5 name">
                                <strong>Time Created:</strong>
                            </div>
                            <div class="col-md-7 value">
                                <?= $model->create_time ?>
                                (<?= \app\assets\Misc::time_elapsed_string($model->create_time) ?>)
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-5 name">
                                <strong>Last Login:</strong>
                            </div>
                            <div class="col-md-7 value">
                                <?= $model->last_login ?>
                                (<?= \app\assets\Misc::time_elapsed_string($model->last_login) ?>)
                            </div>
                        </div>

                        <div class="row mb-2">
                            <div class="col-md-5 name">
                                <strong>Role:</strong>
                            </div>
                            <div class="col-md-7 value">
                                <?= $model->role_name ?>
                            </div>
                        </div>                        
                        <div class="row mb-2">
                            <div class="col-md-5 name">
                                <strong>Verified:</strong>
                            </div>
                            <div class="col-md-7 value">
                                <?= isset($businessProfile->verified) ? ($businessProfile->verified) ? "YES" : "NO" : "NO" ?>
                            </div>
                        </div>                        
                    </div>
                </div>
                <?php if (isset($businessProfile)) { ?>
                    <hr class="my-4" />
                    <h6 class="heading-small text-muted mb-4">Profile</h6>
                    <div class="pl-lg-4">
                        <div class="portlet-body">

                            <div class="row mb-2">
                                <div class="col-md-2 name">
                                    <strong>Business Name:</strong>
                                </div>
                                <div class="col-md-3 value">
                                    <?= isset($businessProfile->business_name) ? "" : "" ?>
                                </div>

                            </div>
                            <div class="row mb-2">
                                <div class="col-md-2 name">
                                    <strong>Business Summary:</strong>
                                </div>
                                <div class="col-md-3 value">
                                    <?= $businessProfile->about_business ?>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-2 name">
                                    <strong>Address:</strong>
                                </div>
                                <div class="col-md-3 value">
                                    <?= $businessProfile->street ?>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-2 name">
                                    <strong>Area:</strong>
                                </div>
                                <div class="col-md-3 value">
                                    <?= $businessProfile->area ?>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-2 name">
                                    <strong>Business Logo:</strong>
                                </div>
                                <div class="col-md-3 value">
                                    <img id="businessLogo" width="200px" src="https://via.placeholder.com/200x200.png?text=Your+Business+Logo" >
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-2 name">
                                    <strong>Type of ID:</strong>
                                </div>
                                <div class="col-md-3 value">
                                    <?= isset($businessProfile->id_type) ? $businessProfile->id_type . '(' . $businessProfile->id_number . ')' : "No ID" ?>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-2 name">
                                    <strong>ID Attachment:</strong>
                                </div>
                                <div class="col-md-3 value">
                                    <img id="uploadLogo" width="200px" src="https://via.placeholder.com/200x200.png?text=Your+ID+Logo" >
                                </div>
                            </div>
                            


                        </div>
                    </div>
                <?php } else { ?>
                    No Business Profile
                <?php } ?>
            </div>
        </div>
    </div>
</div>


<script>

    $(document).ready(function () {

<?php $business_logo = isset($businessProfile->business_logo) ? $businessProfile->business_logo : "" ?>
<?php $id_photo = isset($businessProfile->id_photo) ? $businessProfile->id_photo : "" ?>
        $.get("<?php echo Yii::getAlias('@web') . '/open-file/get-base?img=' . $business_logo . '&dir=' . 'logo' ?>", function (data) {
            //console.log(data);
            if (data != 0) {
                document.getElementById("uploadLogo").src = data;
                document.getElementById("businessLogo").src = data;
            } else {
                document.getElementById("uploadLogo").src = 'https://via.placeholder.com/200x200.png?text=No+ID+Attachment'
                document.getElementById("businessLogo").src = 'https://via.placeholder.com/200x200.png?text=No+Business+Logo'
            }
        });

        $.get("<?php echo Yii::getAlias('@web') . '/open-file/get-base?img=' . $id_photo . '&dir=' . 'idphoto' ?>", function (data) {
            //console.log(data);
            if (data != 0) {
                document.getElementById("uploadLogoId").src = data;
            } else {
                document.getElementById("uploadLogoId").src = 'https://via.placeholder.com/200x200.png?text=ID+Attachment'
            }
        });

//$('#').attr('src',"")

    });

    function readURL(input, id, dataInput) {

        var MAX_FILE_SIZE = 2000000;

        if (input.files && input.files[0]) {
            if (input.files[0].size <= MAX_FILE_SIZE) {
                var reader = new FileReader();

                reader.onload = function (e) {

                    $('#' + id).attr('src', e.target.result);
                    $("#" + dataInput).val(e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            } else {
                $('#' + id).attr('src', "https://via.placeholder.com/200x200.png");
                $("#" + dataInput).val("");
            }
        }
    }
</script>

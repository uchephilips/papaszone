<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\AppUsersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="app-users-search">

    <?php
    $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
    ]);
    ?>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'first_name') ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'last_name') ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'phone_number') ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'email') ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'role_id')->dropDownList([],['class' => 'role_dropdown form-control', 'prompt' => '- Roles -', 'style' => 'width:100%;']) ?>
        </div>
    </div>
    <?php // echo $form->field($model, 'create_time') ?>
    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

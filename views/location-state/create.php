<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LocationState */

$this->title = 'Create Location State';
$this->params['breadcrumbs'][] = ['label' => 'Location States', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="location-state-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

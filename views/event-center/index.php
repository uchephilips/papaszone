<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\EventCentersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Event Centers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-center-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Event Center', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="table-responsive">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                'center_name',
                'description',
                'address',
                'phone',
                'default_price',
                //'email:email',
                //'location_lga_id',
                //'location_state_id',
                //'created_by',
                //'created_time',
                //'updated_time',
                //'updated_by',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Actions',
                    'headerOptions' => ['style' => 'color:#337ab7'],
                    'template' => '{images} <br>{event-center-price} <br>{view} <br> {update} <br> {delete}',
                    'buttons' => [
                        'images' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon glyphicon-camera"></span>', $url, [
                                        'title' => Yii::t('app', 'lead-images'),
                            ]);
                        },
                        'event-center-price' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-euro"></span>', $url, [
                                        'title' => Yii::t('app', 'lead-event-center-price'),
                            ]);
                        },
                        'view' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                        'title' => Yii::t('app', 'lead-view'),
                            ]);
                        },
                        'update' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                        'title' => Yii::t('app', 'lead-update'),
                            ]);
                        },
                        'delete' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                        'title' => Yii::t('app', 'lead-delete'),
                            ]);
                        }
                    ],
                    'urlCreator' => function ($action, $model, $key, $index) {
                        if ($action === 'images') {
                            $url = Yii::getAlias('@web') . '/' . 'event-center-images?eventCenterId=' . $model->id;
                            return $url;
                        }
                        if ($action === 'event-center-price') {
                            $url = Yii::getAlias('@web') . '/' . 'event-center-price?eventCenterId=' . $model->id;
                            return $url;
                        }
                        if ($action === 'view') {
                            $url = Yii::getAlias('@web') . '/' . 'event-center/' . $model->id;
                            return $url;
                        }

                        if ($action === 'update') {
                            $url = Yii::getAlias('@web') . '/' . 'event-center/update/' . $model->id;
                            return $url;
                        }
                        if ($action === 'delete') {
                            $url = Yii::getAlias('@web') . '/' . 'event-center/delete/' . $model->id;
                            return $url;
                        }
                    }
                ],
            ],
        ]);
        ?>
    </div>
</div>

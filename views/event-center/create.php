<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EventCenter */

$this->title = 'Create Event Center';
$this->params['breadcrumbs'][] = ['label' => 'Event Centers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-center-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

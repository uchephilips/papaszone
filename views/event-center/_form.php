<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EventCenter */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
    <div class="col-md-7">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'center_name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>

        <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'default_price')->textInput(['maxlength' => true,'type'=>'number']) ?>

        <?=
        $form->field($model, 'location_state_id')->dropDownList(\yii\helpers\ArrayHelper::map(app\models\LocationState::find()->all(), 'id', 'name')
                , ['prompt' => 'select state', 'id' => 'select_state_id', 'class' => 'form-control form-control-alternative', 'required' => 'required']);
        ?>

        <?=
        $form->field($model, 'location_lga_id')->dropDownList([], ['maxlength' => true, 'class' => 'form-control form-control-alternative',
            'id' => 'select_lga_id', 'prompt' => 'select lga', 'id' => 'select_lga_id', 'required' => 'required'])
        ?>


        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>



<script>

    getLga($('#select_state_id').val());

    $('#select_state_id').change(function () {
        $('#loader').show();
        $('#lga_pane').hide();
        var stateId = $(this).val();
        getLga(stateId);
    });

    function getLga(stateId) {
        $.getJSON("<?= Yii::getAlias('@web'); ?>/site/lga-select?stateId=" + stateId, null, function (data) {

            $("#select_lga_id option").remove(); // Remove all <option> child tags.

            $("#select_lga_id").append("<option value='0'>Select LGA&nbsp;</option>")

            $.each(data, function (index, item) { // Iterates through a collection
                if (item.id == <?= isset($appUserBusinessProfile->lga_id) ? $appUserBusinessProfile->lga_id : 0 ?>) {
                    $("#select_lga_id").append($("<option selected></option>").text(" " + item.name).val(item.id));
                } else {
                    $("#select_lga_id").append($("<option></option>").text(" " + item.name).val(item.id));
                }
            });
            $('#loader').hide();
            $('#lga_pane').show();
        });
    }
</script>
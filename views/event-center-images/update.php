<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EventCenterImages */
/* @var $eventCenter app\models\EventCenter */

$this->title = 'Update Event Center Images: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => $eventCenter->center_name . ' Images', 'url' => ['index?eventCenterId=' . $eventCenter->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="event-center-images-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'eventCenter' => $eventCenter,
    ]) ?>

</div>

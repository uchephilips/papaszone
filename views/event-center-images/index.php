<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\EventCenterImagesSearch */
/* @var $eventCenter app\models\EventCenter */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Event Center Images';
$this->params['breadcrumbs'][] = ['label' => 'Event Centers [' . $eventCenter->center_name . ']', 'url' => ['event-center/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-center-images-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <p>
        <?= Html::a('Create Event Center Images', ['create?eventCenterId=' . $eventCenterId], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'img',
                'format' => 'html',
                'label' => 'Image',
                'value' => function ($data) {
                    return Html::img($data['image_url'],
                                    ['width' => '60px']);
                },
            ],
            'image_url:url',
            //'event_center_id',
            //'created_by',
            //'created_time',
            //'updated_time',
            //'updated_by',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EventCenterImages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-center-images-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'image_url')->textInput(['maxlength' => true]) ?>

    <div class="form-group field-eventcenterimages-image_url">
        <img src="<?=$model->image_url?>" id="img-pane" class="col-md-3" style="width: -webkit-fill-available;">
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>

    $("#eventcenterimages-image_url").on('input', function () {
        var imgUrl = $(this).val();
        $("#img-pane").attr("src", imgUrl);
    });

</script>
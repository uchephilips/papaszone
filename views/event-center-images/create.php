<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EventCenterImages */
/* @var $eventCenter app\models\EventCenter */

$this->title = 'Create Event Center Images';
$this->params['breadcrumbs'][] = ['label' => $eventCenter->center_name . ' Images', 'url' => ['index?eventCenterId=' . $eventCenter->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-center-images-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'eventCenter' => $eventCenter,
    ])
    ?>

</div>

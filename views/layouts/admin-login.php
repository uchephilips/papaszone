<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

    <head>
        <meta charset="utf-8"/>
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="<?= Yii::getAlias('@web'); ?>/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?= Yii::getAlias('@web'); ?>/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?= Yii::getAlias('@web'); ?>/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?= Yii::getAlias('@web'); ?>/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <link href="<?= Yii::getAlias('@web'); ?>/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
        <link href="<?= Yii::getAlias('@web'); ?>/assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE LEVEL PLUGIN STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?= Yii::getAlias('@web'); ?>/assets/admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME STYLES -->
        <link href="<?= Yii::getAlias('@web'); ?>/assets/global/css/components-md.css" id="style_components" rel="stylesheet" type="text/css"/>
        <link href="<?= Yii::getAlias('@web'); ?>/assets/global/css/plugins-md.css" rel="stylesheet" type="text/css"/>
        <link href="<?= Yii::getAlias('@web'); ?>/assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<?= Yii::getAlias('@web'); ?>/assets/admin/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="<?= Yii::getAlias('@web'); ?>/assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->



        <link rel="shortcut icon" href="favicon.ico"/>
    </head>
    <body class="page-md login">

        <div class="menu-toggler sidebar-toggler"></div>
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="index.html">
                <img src="<?= Yii::getAlias('@web'); ?>/assets/admindol/img/wd.jpg" style="width: 20%" alt=""/>
            </a>
        </div>
        <!-- END LOGO -->
        <?= $content ?>

        <script src="<?= Yii::getAlias('@web'); ?>/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?= Yii::getAlias('@web'); ?>/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= Yii::getAlias('@web'); ?>/assets/global/scripts/metronic.js" type="text/javascript"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/admin/pages/scripts/login.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <script>
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core componets
                Layout.init(); // init layout
                Login.init();

            });
        </script>

    </body>
</html>

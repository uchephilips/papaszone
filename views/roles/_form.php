<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Roles */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="roles-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'role_name')->textInput(['maxlength' => true]) ?>

    <hr>
    <?php foreach ($pages as $page) { ?>
        <div class="well">
            <div class="row">
                <div class="col-md-3">

                    <label class="inppt_container"><b><?= $page->controller_name ?></b>
                        <input type="checkbox" name="page[]" value="<?= $page->controller_id ?>">
                        <span class="checkmark"></span>
                    </label>

                </div>
            </div>
            <div class="row">
                <?php foreach ($page->getActions() as $actions) { ?>
                    <div class="col-md-2">
                        <label class="inppt_container"><?= $actions->action_name ?>
                            <input type="checkbox" name="actions[]" <?= $actions->hasAction($model->id) ? "checked" : "" ?> value="<?= $actions->id ?>">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

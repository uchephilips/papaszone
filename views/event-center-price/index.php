<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\EventCenterPriceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Event Center Prices';
$this->params['breadcrumbs'][] = ['label' => 'Event Centers [' . $eventCenter->center_name . ']', 'url' => ['event-center/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-center-price-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Event Center Price', ['create?eventCenterId=' . $eventCenterId], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="table-responsive">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                //['class' => 'yii\grid\SerialColumn'],
                //'id',
                //'event_center_id',
                'price',
                [
                    'attribute' => 'from_day_month',
                    'format' => 'html',
                    'label' => 'From Date Range',
                    'value' => function ($data) {
                        $date = new DateTime($data['from_day_month']);
                        return $date->format('d - F - Y');
                    },
                ],
                [
                    'attribute' => 'to_day_month',
                    'format' => 'html',
                    'label' => 'To Date Range',
                    'value' => function ($data) {
                        $date = new DateTime($data['to_day_month']);
                        return $date->format('d - F - Y');
                    },
                ],
                //'created_by',
                'created_time',
                'updated_time',
                //'updated_by',
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        ?>
    </div>
</div>

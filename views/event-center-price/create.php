<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EventCenterPrice */
/* @var $eventCenter app\models\EventCenter */

$this->title = 'Create Event Center Price';
$this->params['breadcrumbs'][] = ['label' => $eventCenter->center_name . ' Images', 'url' => ['index?eventCenterId=' . $eventCenter->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-center-price-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'eventCenter' => $eventCenter,
    ]) ?>

</div>

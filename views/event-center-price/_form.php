<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EventCenterPrice */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="col-md-6">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'price')->textInput(['maxlength' => true, 'type' => 'number']) ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'from_day_month')->textInput(['required' => 'required', 'type' => 'date', 'min' => date('m-d-Y'), 'date-format' => 'DD-MM-YYYY']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'to_day_month')->textInput(['required' => 'required', 'type' => 'date', 'min' => date('m-d-Y'), 'date-format' => 'DD-MM-YYYY']) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

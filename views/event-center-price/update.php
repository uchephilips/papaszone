<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EventCenterPrice */

$this->title = 'Update Event Center Price: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => $eventCenter->center_name . ' Images', 'url' => ['index?eventCenterId=' . $eventCenter->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="event-center-price-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'eventCenter' => $eventCenter,
    ])
    ?>

</div>

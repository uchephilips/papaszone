<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LocationLga */

$this->title = 'Create Location Lga';
$this->params['breadcrumbs'][] = ['label' => 'Location Lgas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="location-lga-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

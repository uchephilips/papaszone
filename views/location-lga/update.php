<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LocationLga */

$this->title = 'Update Location Lga: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Location Lgas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="location-lga-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\assets;

use Yii;

/**
 * Description of RoleManagement
 *
 * @author uchep
 */
class RoleManagement {

    public static function hasPrivilege($action, $allowedActions = null) {
        $allow = RoleManagement::allow($action, $allowedActions);
        if (!$allow) {
            if (!\Yii::$app->user->isGuest) {
                return RoleManagement::checkIfUserHasPrivilege($action->controller->id, $action->id);
            }
            return false;
        }
        return true;
    }

    public static function checkIfUserHasPrivilege($controller, $action) {
        $roleId = \Yii::$app->user->identity->role_id;
        $isActive = \Yii::$app->user->identity->is_active;
        if (!$isActive) {
            Yii::$app->user->logout();
            return Yii::$app->getResponse()->redirect(Yii::$app->getHomeUrl());
          
        }
        $deviceCount = \Yii::$app->db->createCommand('SELECT count(*) FROM role_actions ra,page_actions pa WHERE ra.page_action_id = pa.id and pa.action_status = "ON"'
                        . ' AND ra.role_id = :roleId AND pa.controller_id = :controllerId AND pa.action_id = :actionId')
                ->bindValue(':roleId', $roleId)
                ->bindValue(':controllerId', $controller)
                ->bindValue(':actionId', $action)
                ->queryScalar();
        return $deviceCount > 0;
    }

    public static function searchRolesAsSelectOption($search) {
        $result = isset($search) && strlen($search) > 0 ? \app\models\Roles::searchRolesAsSelectOption($search) : [];
        return $result;
    }

    public static function getRoleSelectData($search) {
        $rolesResult = RoleManagement::searchRolesAsSelectOption($search);

        return ['results' => [
                    ['children' => $rolesResult]
            ]
        ];
    }

    /**
     * 
     * @param type $action
     * @param type $allowedActions
     * @return type
     */
    private static function allow($action, $allowedActions) {
        if (isset($allowedActions)) {
            return strpos($allowedActions, $action->id) !== false;
        }
        return false;
    }

    public static function checkIsCustomer() {
        return RoleManagement::checkIfUserHasPrivilege('app-users', 'is-customer');
    }

    public static function checkSelfEntry() {
        return RoleManagement::checkIfUserHasPrivilege('app-users', 'self-entry');
    }

    public static function checkSelfEntryCreatedById($searchCreatedById) {
        if (RoleManagement::checkIfUserHasPrivilege('app-users', 'self-entry')) {
            return \Yii::$app->user->id;
        } else {
            return $searchCreatedById;
        }
    }

}

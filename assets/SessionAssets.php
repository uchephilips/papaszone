<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\assets;

use Yii;
use app\models\InventoryCheck;
use app\models\ServiceAvailability;
use app\models\ServiceType;
use app\models\Lcan;
use app\models\CustomerDetails;
use app\models\LandlordDetails;
use app\models\ApplicationPayment;
use app\models\CustomerDetailsAttachment;

/**
 * Description of SessionAssets
 *
 * @author uchephilz
 */
class SessionAssets {

    public static function getUserSession() {
        return \Yii::$app->session->get('USER_SESSION');
    }

    public static function setUserSession($userData) {
        return \Yii::$app->session->set('USER_SESSION', $userData);
    }

    public static function hasUserSession() {
        return \Yii::$app->session->has('USER_SESSION');
    }

}

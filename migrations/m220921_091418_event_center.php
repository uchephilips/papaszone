<?php

use yii\db\Migration;

/**
 * Class m220921_091418_event_center
 */
class m220921_091418_event_center extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        echo "m220921_091418_event_center cannot be reverted.\n";

        return false;
    }

    // Use up()/down() to run migration code without a transaction.
    public function up() {
        $this->createTable('event_center', [
            'id' => $this->primaryKey()
        ]);
    }

    /*
      public function down()
      {
      echo "m220921_091418_event_center cannot be reverted.\n";

      return false;
      }
     */
}

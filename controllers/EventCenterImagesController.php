<?php

namespace app\controllers;

use Yii;
use app\models\EventCenterImages;
use app\models\search\EventCenterImagesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EventCenterImagesController implements the CRUD actions for EventCenterImages model.
 */
class EventCenterImagesController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return \app\assets\RoleManagement::hasPrivilege($action);
                            //return true;
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all EventCenterImages models.
     * @return mixed
     */
    public function actionIndex($eventCenterId) {

        $eventCenter = $this->findEventCenterModel($eventCenterId);

        $searchModel = new EventCenterImagesSearch();
        $searchModel->event_center_id = $eventCenterId;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'eventCenter' => $eventCenter,
                    'eventCenterId' => $eventCenterId,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EventCenterImages model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EventCenterImages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new EventCenterImages();

        $eventCenterId = Yii::$app->request->get("eventCenterId");
        $model->event_center_id = $eventCenterId;
        $eventCenter = $this->findEventCenterModel($eventCenterId);

        if ($model->load(Yii::$app->request->post())) {
            $model->event_center_id = $eventCenterId;
            $model->created_by = Yii::$app->user->identity->appUserId;
            $model->created_time = date("Y-m-d H:i:s");

            if (!$model->save()) {
                Yii::$app->session->setFlash('error', $model->getErrorSummary(true));
            } else {
                Yii::$app->session->setFlash('success', "Saved");
                return $this->redirect(['index?eventCenterId=' . $eventCenterId]);
            }
        }

        return $this->render('create', [
                    'eventCenter' => $eventCenter,
                    'model' => $model,
                    'eventCenterId' => $eventCenterId,
        ]);
    }

    /**
     * Updates an existing EventCenterImages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        $eventCenter = $this->findEventCenterModel($model->event_center_id);
        $eventCenterId = $model->event_center_id;
        if ($model->load(Yii::$app->request->post())) {
            $model->updated_by = Yii::$app->user->identity->appUserId;
            $model->updated_time = date("Y-m-d H:i:s");

            if (!$model->save()) {
                Yii::$app->session->setFlash('error', $model->getErrorSummary(true));
            } else {
                Yii::$app->session->setFlash('success', "Saved");
                return $this->redirect(['index?eventCenterId=' . $eventCenterId]);
            }
        }

        return $this->render('update', [
                    'eventCenter' => $eventCenter,
                    'eventCenterId' => $eventCenterId,
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing EventCenterImages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);

        $eventCenterId = $model->event_center_id;

        $model->delete();

        return $this->redirect(['index?eventCenterId=' . $eventCenterId]);
    }

    /**
     * Finds the EventCenterImages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EventCenterImages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = EventCenterImages::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Finds the EventCenter model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \app\models\EventCenter the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findEventCenterModel($id) {
        if (($model = \app\models\EventCenter::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}

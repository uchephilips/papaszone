<?php

namespace app\controllers;

use Yii;
use app\models\Roles;
use app\models\search\RolesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RolesController implements the CRUD actions for Roles model.
 */
class RolesController extends Controller {

        /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return \app\assets\RoleManagement::hasPrivilege($action);
                            //return true;
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Roles models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new RolesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Roles model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Roles model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Roles();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Roles model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        
        
        if ($model->load(Yii::$app->request->post())) {
           
            $model->updated_by = Yii::$app->user->identity->appUserId;
            $model->update_time = date("Y-m-d H:i:s");

            if ($model->save()) {
                \Yii::$app->db->createCommand('delete from role_actions where role_id = :roleId')
                        ->bindValue(':roleId', $id)
                        ->execute();
                foreach (Yii::$app->request->post("actions") as $action) {
                    $pageAction = new \app\models\RoleActions();
                    $pageAction->page_action_id = $action;
                    $pageAction->updated_by = Yii::$app->user->identity->id;
                    $pageAction->update_time = date("Y-m-d H:i:s");
                    $pageAction->created_by = Yii::$app->user->identity->appUserId;
                    $pageAction->created_time = date("Y-m-d H:i:s");
                    $pageAction->role_id = $model->id;
                    if (!$pageAction->save()) {
                        Yii::$app->session->setFlash('error', "Some actions could not be saved");
                        Yii::error($pageAction->getErrorSummary(true));
                    }
                }

                Yii::$app->session->setFlash('success', "Role updated");
                return $this->redirect(['index']);
            } else {
                print_r($model->getErrors());
                exit;
            }
        } else {
            
        }

        $pages = \app\models\PageActions::getPages();

        return $this->render('update', [
                    'model' => $model,
                    'pages' => $pages
        ]);
    }

    /**
     * Deletes an existing Roles model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Roles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Roles the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Roles::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}

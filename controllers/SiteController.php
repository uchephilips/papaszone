<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return \app\assets\RoleManagement::hasPrivilege($action);
                            //return true;
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {

        $options = array("cost" => 12);
        echo password_hash("download", PASSWORD_BCRYPT, $options);

        $this->layout = 'admin-login';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }



        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->login()) {

                //return $this->goBack();
                return $this->redirect(['event-center/index']);
            } else {
                Yii::$app->session->setFlash('error', "Incorrect login details");
            }
        }

        $model->password = '';
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    public function actionRegistration() {

        $model = new \app\models\AppUsers();

        if ($model->load(Yii::$app->request->post())) {
//            print_r($model->password);
//            exit;
            if ($_POST['c_p'] == $model->password) {

                $options = array("cost" => 12);
                $model->password = password_hash($model->password, PASSWORD_BCRYPT, $options);

                if ($model->save()) {
                    Yii::$app->session->setFlash('success', "User created successfully.");
                    return $this->redirect(['index']);
                } else {
                    Yii::$app->session->setFlash('error', "User not successful.");
                }
            } else {
                Yii::$app->session->setFlash('error', "Password doesn't match");
            }
        }

        return $this->redirect(['index']);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout() {
        return $this->render('about');
    }

    public function actionLgaSelect() {
        $stateId = \Yii::$app->request->get('stateId');
        $res = \app\models\LocationLga::find()->select(['id', 'name'])->where(['state_id' => $stateId])->asArray(true)->all();
        echo json_encode($res);
    }

}

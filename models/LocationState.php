<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "location_state".
 *
 * @property int $id
 * @property string $name
 * @property int $created_by
 * @property string $created_time
 * @property string $updated_time
 * @property int $updated_by
 *
 * @property EventCenter[] $eventCenters
 * @property LocationLga[] $locationLgas
 */
class LocationState extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'location_state';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'created_by', 'created_time'], 'required'],
            [['created_by', 'updated_by'], 'integer'],
            [['created_time', 'updated_time'], 'safe'],
            [['name'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventCenters()
    {
        return $this->hasMany(EventCenter::className(), ['location_state_id' => 'id'])->inverseOf('locationState');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocationLgas()
    {
        return $this->hasMany(LocationLga::className(), ['state_id' => 'id'])->inverseOf('state');
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\LocationStateQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\LocationStateQuery(get_called_class());
    }
}

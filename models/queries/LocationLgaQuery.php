<?php

namespace app\models\queries;

/**
 * This is the ActiveQuery class for [[\app\models\LocationLga]].
 *
 * @see \app\models\LocationLga
 */
class LocationLgaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\models\LocationLga[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\LocationLga|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

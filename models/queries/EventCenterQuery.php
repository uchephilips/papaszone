<?php

namespace app\models\queries;

/**
 * This is the ActiveQuery class for [[\app\models\EventCenter]].
 *
 * @see \app\models\EventCenter
 */
class EventCenterQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\models\EventCenter[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\EventCenter|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

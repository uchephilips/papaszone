<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "location_lga".
 *
 * @property int $id
 * @property string $name
 * @property int $state_id
 * @property int $created_by
 * @property string $created_time
 * @property string $updated_time
 * @property int $updated_by
 *
 * @property EventCenter[] $eventCenters
 * @property LocationState $state
 */
class LocationLga extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'location_lga';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['name', 'state_id', 'created_by', 'created_time'], 'required'],
            [['state_id', 'created_by', 'updated_by'], 'integer'],
            [['created_time', 'updated_time'], 'safe'],
            [['name'], 'string', 'max' => 20],
            [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => LocationState::className(), 'targetAttribute' => ['state_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'state_id' => 'State',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventCenters() {
        return $this->hasMany(EventCenter::className(), ['location_lga_id' => 'id'])->inverseOf('locationLga');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocationState() {
        return $this->hasOne(LocationState::className(), ['id' => 'state_id'])->inverseOf('locationLgas');
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\LocationLgaQuery the active query used by this AR class.
     */
    public static function find() {
        return new \app\models\queries\LocationLgaQuery(get_called_class());
    }

}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "event_center_images".
 *
 * @property int $id
 * @property string $image_url
 * @property int $event_center_id
 * @property int $created_by
 * @property string $created_time
 * @property string $updated_time
 * @property int $updated_by
 */
class EventCenterImages extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'event_center_images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['image_url', 'event_center_id', 'created_by', 'created_time'], 'required'],
            [['event_center_id', 'created_by', 'updated_by'], 'integer'],
            [['created_time', 'updated_time'], 'safe'],
            [['image_url'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'image_url' => 'Image Link',
            'event_center_id' => 'Event Center',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
            'updated_by' => 'Updated By',
        ];
    }

}

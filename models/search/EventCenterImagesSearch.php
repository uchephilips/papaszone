<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EventCenterImages;

/**
 * EventCenterImagesSearch represents the model behind the search form of `app\models\EventCenterImages`.
 */
class EventCenterImagesSearch extends EventCenterImages {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'event_center_id', 'created_by', 'updated_by'], 'integer'],
            [['image_url', 'created_time', 'updated_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = EventCenterImages::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!\Yii::$app->request->get("sort",false)) {
            $query->orderBy(['id' => SORT_DESC]);
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'event_center_id' => $this->event_center_id,
            'created_by' => $this->created_by,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'image_url', $this->image_url]);

        return $dataProvider;
    }

}

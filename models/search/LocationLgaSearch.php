<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LocationLga;

/**
 * LocationLgaSearch represents the model behind the search form of `app\models\LocationLga`.
 */
class LocationLgaSearch extends LocationLga {

    public $stateName;

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'state_id', 'created_by', 'updated_by'], 'integer'],
            [['name', 'created_time', 'updated_time', 'stateName'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = LocationLga::find();

        // add conditions that should always apply here

        $query->joinWith(['locationState']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['stateName'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['location_state.name' => SORT_ASC],
            'desc' => ['location_state.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'state_id' => $this->state_id,
            'created_by' => $this->created_by,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
            'updated_by' => $this->updated_by,
        ]);

        if (!\Yii::$app->request->get("sort",false)) {
            $query->orderBy(['id' => SORT_DESC]);
        }

        $query->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'location_state.name', $this->stateName]);

        return $dataProvider;
    }

}

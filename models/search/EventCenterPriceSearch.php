<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EventCenterPrice;

/**
 * EventCenterPriceSearch represents the model behind the search form of `app\models\EventCenterPrice`.
 */
class EventCenterPriceSearch extends EventCenterPrice {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'event_center_id', 'created_by', 'updated_by'], 'integer'],
            [['price', 'from_day_month', 'to_day_month', 'created_time', 'updated_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = EventCenterPrice::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'event_center_id' => $this->event_center_id,
            'created_by' => $this->created_by,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
            'updated_by' => $this->updated_by,
        ]);

        if (!\Yii::$app->request->get("sort",false)) {
            $query->orderBy(['id' => SORT_DESC]);
        }

        $query->andFilterWhere(['like', 'price', $this->price])
                ->andFilterWhere(['like', 'from_day_month', $this->from_day_month])
                ->andFilterWhere(['like', 'to_day_month', $this->to_day_month]);

        return $dataProvider;
    }

}

<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EventCenter;

/**
 * EventCentersSearch represents the model behind the search form of `app\models\EventCenter`.
 */
class EventCentersSearch extends EventCenter {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'location_lga_id', 'location_state_id', 'created_by', 'updated_by'], 'integer'],
            [['center_name', 'description', 'address', 'phone', 'email', 'created_time', 'updated_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = EventCenter::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'location_lga_id' => $this->location_lga_id,
            'location_state_id' => $this->location_state_id,
            'created_by' => $this->created_by,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
            'updated_by' => $this->updated_by,
        ]);

        if (!\Yii::$app->request->get("sort",false)) {
            $query->orderBy(['id' => SORT_DESC]);
        }

        $query->andFilterWhere(['like', 'center_name', $this->center_name])
                ->andFilterWhere(['like', 'description', $this->description])
                ->andFilterWhere(['like', 'address', $this->address])
                ->andFilterWhere(['like', 'phone', $this->phone])
                ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }

}

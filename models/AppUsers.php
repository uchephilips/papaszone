<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "app_users".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $phone_number
 * @property string $email
 * @property string $create_time
 * @property string $last_update
 * @property string $password
 * @property bool $forget_password
 * @property string $access_token
 * @property bool $is_active
 * @property int $role_id
 * @property int $created_by
 * @property int $updated_by
 * @property string $last_login
 * @property string $is_active
 */
class AppUsers extends \yii\db\ActiveRecord {

    public $confirm_password;
    public $role_name;
    public $verified;

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'app_users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['first_name', 'last_name', 'is_active', 'role_id', 'phone_number', 'email', 'create_time', 'password'], 'required'],
            [['create_time', 'last_update', 'last_login'], 'safe'],
            [['forget_password', 'is_active'], 'boolean'],
            [['first_name', 'last_name'], 'string', 'max' => 20],
            [['email'], 'string', 'max' => 30],
            [['phone_number'], 'string', 'max' => 11],
            [['password', 'access_token'], 'string', 'max' => 150],
            [['email'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'phone_number' => 'Phone Number',
            'email' => 'Email',
            'create_time' => 'Create Time',
            'last_update' => 'Last Update',
            'password' => 'Password',
            'forget_password' => 'Forget Password',
            'access_token' => 'Access Token',
            'is_active' => 'Is Active',
            'last_login' => 'Last Login',
        ];
    }

 

    public function getRoleName() {
        $role = Roles::findOne(['id' => $this->role_id]);
        if (isset($role)) {
            return $role->role_name;
        } else {
            return "not set";
        }
    }

    public function getFullname() {
        return $this->first_name . ' ' . $this->last_name;
    }

}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "event_center_price".
 *
 * @property int $id
 * @property int $event_center_id
 * @property string $price
 * @property string $from_day_month
 * @property string $to_day_month
 * @property int $created_by
 * @property string $created_time
 * @property string $updated_time
 * @property int $updated_by
 */
class EventCenterPrice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_center_price';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event_center_id', 'price', 'created_by', 'created_time'], 'required'],
            [['event_center_id', 'created_by', 'updated_by'], 'integer'],
            [['created_time', 'updated_time'], 'safe'],
            [['price'], 'string', 'max' => 1000],
            [['from_day_month', 'to_day_month'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_center_id' => 'Event Center',
            'price' => 'Price (₦)',
            'from_day_month' => 'From Day Month',
            'to_day_month' => 'To Day Month',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
            'updated_by' => 'Updated By',
        ];
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "amenity".
 *
 * @property int $id
 * @property string $name
 * @property int $created_by
 * @property string $created_time
 * @property string $updated_time
 * @property int $updated_by
 */
class Amenity extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'amenity';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['name', 'created_by', 'created_time'], 'required'],
            [['created_by', 'updated_by'], 'integer'],
            [['created_time', 'updated_time'], 'safe'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
            'updated_by' => 'Updated By',
        ];
    }

}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "event_center".
 *
 * @property int $id
 * @property string $center_name
 * @property string $description
 * @property string $address
 * @property string $phone
 * @property string $default_price
 * @property string $email
 * @property int $location_lga_id
 * @property int $location_state_id
 * @property int $created_by
 * @property string $created_time
 * @property string $updated_time
 * @property int $updated_by
 *
 * @property LocationLga $locationLga
 * @property LocationState $locationState
 */
class EventCenter extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_center';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['center_name', 'description', 'address', 'phone', 'default_price', 'email', 'location_lga_id', 'location_state_id', 'created_by', 'created_time'], 'required'],
            [['location_lga_id', 'location_state_id', 'created_by', 'updated_by'], 'integer'],
            [['created_time', 'updated_time'], 'safe'],
            [['center_name'], 'string', 'max' => 200],
            [['description'], 'string', 'max' => 4000],
            [['address', 'phone', 'email'], 'string', 'max' => 200],
            [['default_price'], 'string', 'max' => 10],
            [['location_lga_id'], 'exist', 'skipOnError' => true, 'targetClass' => LocationLga::className(), 'targetAttribute' => ['location_lga_id' => 'id']],
            [['location_state_id'], 'exist', 'skipOnError' => true, 'targetClass' => LocationState::className(), 'targetAttribute' => ['location_state_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'center_name' => 'Center Name',
            'description' => 'Description',
            'address' => 'Address',
            'phone' => 'Phone',
            'default_price' => 'Default Price (₦)',
            'email' => 'Email',
            'location_lga_id' => 'Location Lga ID',
            'location_state_id' => 'Location State ID',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocationLga()
    {
        return $this->hasOne(LocationLga::className(), ['id' => 'location_lga_id'])->inverseOf('eventCenters');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocationState()
    {
        return $this->hasOne(LocationState::className(), ['id' => 'location_state_id'])->inverseOf('eventCenters');
    }
}
